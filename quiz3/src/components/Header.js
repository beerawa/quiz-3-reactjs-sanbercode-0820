import React from "react";
// import TemaContext from "./TemaContext";
import { Link } from "react-router-dom";
import "../css/style.css";

const Header = () => {
  //   const tema = useContext(TemaContext);
  return (
    <div>
      <header>
        <nav>
          <img
            src={require("../img/logo.png")}
            id="logo"
            width="200px"
            alt="a"
          />
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/contact">Contact</Link>
            </li>
            <li>
              <Link to="/movies">Movie List Editor</Link>
            </li>
            <li>
              <Link to="/login">Login</Link>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  );
};

export default Header;
