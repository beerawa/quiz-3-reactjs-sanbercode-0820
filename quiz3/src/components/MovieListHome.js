import React, { useContext, useEffect } from "react";
import { MovieContext } from "./MovieContext";
import axios from "axios";

const MovieListHome = () => {
  const [movie, setMovie, inputForm, setInputForm] = useContext(MovieContext);

  useEffect(() => {
    if (movie === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/movies`)
        .then((res) => {
          setMovie(res.data);
        });
    }
  }, [movie]);

  return (
    <div>
      <div style={{ marginTop: 100 }}></div>
      <div
        style={{
          backgroundColor: "white",
          width: 1200,
          paddingLeft: 50,
          paddingRight: 50,
          display: "inline-block",
          textAlign: "center",
        }}
      >
        <h1>Daftar Film-film Terbaik</h1>
        {movie !== null &&
          movie.map((el, idx) => {
            return (
              <div>
                <h3 style={{ textAlign: "left" }}>{el.title}</h3>
                <div style={{ display: "inline-flex" }}>
                  <table>
                    <tr>
                      <td>
                        <img
                          style={{
                            width: 950,
                            height: 500,
                            objectFit: "cover",
                            flex: 4,
                          }}
                          src={el.image_url}
                          id="logo"
                        />
                      </td>
                      <td>
                        <div
                          style={{ flex: 1, padding: 50, textAlign: "left" }}
                        >
                          <p>
                            <strong>Rating {el.rating}</strong>
                          </p>
                          <p>
                            <strong>Durasi: {el.duration}</strong>
                          </p>
                          <p>
                            <strong>Genre: {el.genre}</strong>
                          </p>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <p style={{ textAlign: "left" }}>
                  <strong>Deskripsi</strong> {el.description}
                </p>
                {/* <div style={{ borderColor: "black", borderTop: 1 }}></div> */}
                <hr size="10px"></hr>
              </div>
            );
          })}
      </div>
      <div style={{ marginBottom: 100 }}></div>
    </div>
  );
};

export default MovieListHome;
