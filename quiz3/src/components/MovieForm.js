import React, { useContext, useEffect } from "react";
import { MovieContext } from "./MovieContext";
import axios from "axios";

const MovieForm = () => {
  const [movie, setMovie, inputForm, setInputForm] = useContext(MovieContext);

  useEffect(() => {
    if (movie === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/movies`)
        .then((res) => {
          setMovie(res.data);
        });
    }
  }, [movie, setMovie]);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (inputForm.id === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/movies`, {
          title: inputForm.title,
          description: inputForm.description,
          year: inputForm.year,
          duration: inputForm.duration,
          genre: inputForm.genre,
          rating: inputForm.rating,
          review: inputForm.review,
          image_url: inputForm.image_url,
        })
        .then((res) => {
          let data = res.data;
          setMovie([
            ...movie,
            {
              id: data.id,
              title: data.title,
              description: data.description,
              year: data.year,
              duration: data.duration,
              genre: data.genre,
              rating: data.rating,
              review: data.review,
              image_url: data.image_url,
            },
          ]);
          setInputForm({
            id: null,
            title: "",
            description: "",
            year: 0,
            duration: 0,
            genre: "",
            rating: 0,
            review: "",
            image_url: "",
          });
        });
    } else {
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/movies/${inputForm.id}`,
          {
            title: inputForm.title,
            description: inputForm.description,
            year: inputForm.year,
            duration: inputForm.duration,
            genre: inputForm.genre,
            rating: inputForm.rating,
            review: inputForm.review,
            image_url: inputForm.image_url,
          }
        )
        .then((res) => {
          let newDataMovie = movie.map((x) => {
            if (x.id === inputForm.id) {
              x.title = inputForm.title;
              x.description = inputForm.description;
              x.year = inputForm.year;
              x.duration = inputForm.duration;
              x.genre = inputForm.genre;
              x.rating = inputForm.rating;
              x.review = inputForm.review;
              x.image_url = inputForm.image_url;
            }
            return x;
          });
          setMovie(newDataMovie);
          setInputForm({
            id: null,
            title: "",
            description: "",
            year: 0,
            duration: 0,
            genre: "",
            rating: 0,
            review: "",
            image_url: "",
          });
        });
    }
  };

  const handleChangeTitle = (event) => {
    setInputForm({ ...inputForm, title: event.target.value });
  };

  const handleChangeDescription = (event) => {
    setInputForm({ ...inputForm, description: event.target.value });
  };

  const handleChangeYear = (event) => {
    setInputForm({ ...inputForm, year: event.target.value });
  };

  const handleChangeDuration = (event) => {
    setInputForm({ ...inputForm, duration: event.target.value });
  };

  const handleChangeGenre = (event) => {
    setInputForm({ ...inputForm, genre: event.target.value });
  };

  const handleChangeRating = (event) => {
    setInputForm({ ...inputForm, rating: event.target.value });
  };

  const handleChangeImageUrl = (event) => {
    setInputForm({ ...inputForm, image_url: event.target.value });
  };

  return (
    <>
      <div
        style={{
          backgroundColor: "white",
          width: 1400,
          padding: 50,
          display: "inline-block",
          textAlign: "center",
        }}
      >
        <h1>Movies Form</h1>
        <form onSubmit={handleSubmit}>
          <div
            style={{
              display: "inline-block",
              textAlign: "center",
              width: 700,
            }}
          >
            <table style={{ width: "80%" }}>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Title : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <input
                    type="text"
                    value={inputForm.title}
                    onChange={handleChangeTitle}
                    size="30"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Description : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <textarea
                    value={inputForm.description}
                    onChange={handleChangeDescription}
                    rows="4"
                    cols="25"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Year : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <input
                    type="number"
                    value={inputForm.year}
                    onChange={handleChangeYear}
                    size="10p"
                    style={{ width: 90 }}
                    min="1980"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Duration : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <input
                    type="number"
                    value={inputForm.duration}
                    onChange={handleChangeDuration}
                    style={{ width: 200 }}
                    min="0"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Genre : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <input
                    type="text"
                    value={inputForm.genre}
                    onChange={handleChangeGenre}
                    size="10"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Rating : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <input
                    type="number"
                    value={inputForm.rating}
                    onChange={handleChangeRating}
                    style={{ width: 40 }}
                    min="0"
                    max="10"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td style={{ textAlign: "left" }}>
                  <strong>Image Url : </strong>
                </td>
                <td style={{ textAlign: "right" }}>
                  <textarea
                    type="text"
                    value={inputForm.image_url}
                    onChange={handleChangeImageUrl}
                    row="4"
                    cols="55"
                    required
                  />
                </td>
              </tr>
              <tr>
                <td></td>
                <td style={{ textAlign: "right" }}>
                  <button>submit</button>
                </td>
              </tr>
            </table>
          </div>
        </form>
      </div>
    </>
  );
};

export default MovieForm;
