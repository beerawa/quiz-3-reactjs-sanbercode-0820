import React, { useState, createContext } from "react";
// import axios from "axios";
// import "./DaftarBuah.css";

export const MovieContext = createContext();

export const MovieProvider = (props) => {
  const [movie, setMovie] = useState(null);

  const [inputForm, setInputForm] = useState({
    // name: "",
    // lengthOfTime: 0,
    // id: null,
    title: "",
    description: "",
    year: 2020,
    duration: 120,
    genre: "",
    rating: 0,
    review: "",
    image: "",
    id: null,
    search: "",
  });

  return (
    <MovieContext.Provider value={[movie, setMovie, inputForm, setInputForm]}>
      {props.children}
    </MovieContext.Provider>
  );
};
