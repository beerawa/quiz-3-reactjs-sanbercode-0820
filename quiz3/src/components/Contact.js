import React from "react";

const Contact = () => {
  //   const tema = useContext(TemaContext);
  return (
    <div>
      <section>
        <h1>Contact</h1>
        <div id="article-list">
          <div class="article">
            <a href="">
              <h3>Lorem Post 1</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div class="article">
            <a href="">
              <h3>Lorem Post 2</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div class="article">
            <a href="">
              <h3>Lorem Post 3</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div class="article">
            <a href="">
              <h3>Lorem Post 4</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div class="article">
            <a href="">
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href="">
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href="">
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href="">
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href="">
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href="">
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant
              eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque
              delicatissimi ut. Et sea quem sint, nam in minim voluptatibus.
              Etiam placerat eam in.
            </p>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Contact;
