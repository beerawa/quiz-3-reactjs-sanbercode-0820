import React, { useState, useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { AuthContext, useAuth } from "./AuthContext";

// import Movies from "./Movie";

// function RoutesPrivate({ component: Component, ...rest }) {
function RoutesPrivate({ component: Component, ...rest }) {
  // const [auth, setAuth, inputForm, setInputForm] = useContext(AuthContext);
  const isAuthenticated = useAuth();
  // const isAuthenticated = auth;

  // alert(auth.auth);

  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
}

export default RoutesPrivate;
