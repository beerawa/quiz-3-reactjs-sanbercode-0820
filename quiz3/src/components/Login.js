import React, { useState, useContext } from "react";
import { Redirect } from "react-router-dom";
import { AuthContext, useAuth } from "./AuthContext";

function Login() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthTokens } = useAuth();
  // const [auth, setAuth, inputForm, setInputForm] = useContext(AuthContext);

  function postLogin(e) {
    e.preventDefault();
    if (userName === "admin" && password === "admin") {
      setAuthTokens(undefined);
      // setAuth(true);
      setLoggedIn(true);
      // useContext(AuthContext);
    }
  }

  if (isLoggedIn) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <div style={{ marginTop: 100 }} />
      <form onSubmit={postLogin}>
        <input
          type="username"
          value={userName}
          onChange={(e) => {
            setUserName(e.target.value);
          }}
          placeholder="username"
        />
        <input
          type="password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <button>Sign In</button>
      </form>
    </div>
  );
}

export default Login;
