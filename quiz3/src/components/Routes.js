import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import RoutesPrivate from "./RoutesPrivate";
import { AuthContext } from "./AuthContext";

import Header from "./Header";
import "../css/style.css";

import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import Movies from "./Movie";
import Login from "./Login";

export default function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  };

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      {/* // <AuthContext.Provider value={authTokens}> */}
      <Router>
        <div>
          {/* <TemaContext.Provider value={temaa}> */}
          <Header />
          {/* </TemaContext.Provider> */}
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={About} />
            <Route exact path="/contact" component={Contact} />
            <RoutesPrivate exact path="/movies" component={Movies} />
            <Route exact path="/login">
              <Login />
            </Route>

            {/* <Route exact path="/tugas15">
            <TemaContext.Provider value={temaa}>
              <button onClick={toggleTema}>Ubah Tema</button>
            </TemaContext.Provider>
          </Route> */}
          </Switch>
          <footer>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
          </footer>
        </div>
      </Router>
    </AuthContext.Provider>
  );
}
