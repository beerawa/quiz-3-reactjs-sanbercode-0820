import React, { createContext, useContext, useState } from "react";

export const AuthContext = createContext(false);

export function useAuth() {
  return useContext(AuthContext);
}

export const AuthProvider = (props) => {
  const [auth, setAuth] = useState(null);

  const [inputForm, setInputForm] = useState({
    auth: false,
  });

  return (
    <AuthContext.Provider value={[auth, setAuth, inputForm, setInputForm]}>
      {props.children}
    </AuthContext.Provider>
  );
};
