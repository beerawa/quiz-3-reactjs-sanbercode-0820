import React from "react";
import { MovieProvider } from "./MovieContext";
import MovieList from "./MovieList";
import MovieForm from "./MovieForm";

const Home = () => {
  //   const tema = useContext(TemaContext);

  return (
    <div>
      <div style={{ marginTop: 100 }}></div>
      <MovieProvider>
        <MovieList />
        <MovieForm />
      </MovieProvider>
      <div style={{ marginBottom: 100 }}></div>
    </div>
  );
};

export default Home;
