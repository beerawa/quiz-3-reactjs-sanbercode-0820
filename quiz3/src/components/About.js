import React from "react";
import "../css/style.css";

const About = () => {
  //   const tema = useContext(TemaContext);
  return (
    <div>
      <div
        style={{
          marginTop: 100,
          display: "inline-block",
          textAlign: "center",
          padding: 10,
          border: 1,
          backgroundColor: "white",
          width: 700,
        }}
      >
        <div
          style={{
            margin: 10,
            border: 1,
            backgroundColor: "white",
            width: 680,
            borderColor: "grey",
            borderRadius: 10,
            borderWidth: 1,
            borderStyle: "solid",
          }}
        >
          <h1 style={{ textAlign: "center" }}>
            Data Peserta Sanbercode Bootcamp Reactjs
          </h1>
          <ol style={{ textAlign: "left" }}>
            <li>
              <strong style={{ width: 100 }}>Nama:</strong> I Nyoman Ega Beerawa
            </li>
            <li>
              <strong style={{ width: 100 }}>Email:</strong> birrawzky@gmail.com
            </li>
            <li>
              <strong style={{ width: 100 }}>
                Sistem Operasi yang digunakan:
              </strong>{" "}
              Microsoft Windows 10
            </li>
            <li>
              <strong style={{ width: 100 }}>Akun Gitlab:</strong> @beerawa
            </li>
            <li>
              <strong style={{ width: 100 }}>Akun Telegram:</strong> @egabeerawa
            </li>
          </ol>
        </div>
      </div>
    </div>
  );
};

export default About;
