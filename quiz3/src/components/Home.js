import React from "react";
import { MovieProvider } from "./MovieContext";
import MovieListHome from "./MovieListHome";

const Home = () => {
  //   const tema = useContext(TemaContext);

  return (
    <div>
      <MovieProvider>
        <MovieListHome />
      </MovieProvider>
    </div>
  );
};

export default Home;
