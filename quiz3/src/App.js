import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Aplikasi from "./components/Routes";
// import Header from "./components/Header";
import "./css/style.css";

import "./App.css";

function App() {
  return (
    <div className="App">
      <Aplikasi />
    </div>
  );
}

export default App;
